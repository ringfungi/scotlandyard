package com.scotlandyard.scotlandyard;

import java.time.LocalDate;
import java.util.*;

public class JailingService {

    Node<Politician> rootNode;
    Boolean isOnlyChild = Boolean.FALSE;
    Node<Politician> superior;
    Set<Politician> subordinates;

    public JailingService(Node<Politician> rootNode) {
        this.rootNode = rootNode;
    }

    void jail(Politician politician)  {
        List<Node<Politician>> goalAndOldest = BreadthFirstSearch(rootNode, politician.getId());

        System.out.println("Goal:" + goalAndOldest.get(0).getData().getId() + " " + "Oldest brother:" + goalAndOldest.get(1).getData().getId() + "\n");

        //If they have no brothers, promote oldest child
        if (isOnlyChild) { 
            Node<Politician> oldestChild = getOldestChild(goalAndOldest.get(0));
            List<Node<Politician>> childrenToUpdate = goalAndOldest.get(0).deleteNodePromoteChild(oldestChild);

            for (Node<Politician> node : childrenToUpdate) {
                node.getData().setIdSup(oldestChild.getData().getId());
            }

        } else /*if ((goalAndOldest.get(0).getData().getId() != goalAndOldest.get(1).getData().getId()))*/{
        //Transfer all the children to oldest brother
            List<Node<Politician>> childrenToUpdate = goalAndOldest.get(0).deleteNodeTransferChildren(goalAndOldest.get(1));

            for (Node<Politician> node : childrenToUpdate) {
                node.getData().setIdSup(goalAndOldest.get(1).getData().getId());
            }
        }
    }

    void free(Politician politician)  {

    }

    public Node<Politician> getOldestChild(Node<Politician> rootNode) {

        if (rootNode == null)
            return null;
        Queue<Node<Politician>> queue = new LinkedList<>();
        queue.add(rootNode);

        for (Node<Politician> node : new ArrayList<>(queue) ) {

            if (node.getChildren() != null)
                queue.addAll(node.getChildren());

            queue.poll();
        }

        LocalDate earliestDate =  LocalDate.MAX;
        Node<Politician> mostSeniorChild = new Node<Politician>();

        //Check the oldest child
        for (Node<Politician> node : queue) {
            if (node.getData().getSeniority().isBefore(earliestDate)) {
                earliestDate = node.getData().getSeniority();
                mostSeniorChild = node;
            }
        }
        return mostSeniorChild;
    }



    public List<Node<Politician>> BreadthFirstSearch(Node<Politician> rootNode, Integer goalId)  {

        Boolean foundGoal = Boolean.FALSE;
        List<Node<Politician>> goalAndOldest = new ArrayList<Node<Politician>>();

        if (rootNode == null)
            return null;

        Queue<Node<Politician>> queue = new LinkedList<>();
        queue.add(rootNode);

        while (!queue.isEmpty()) {
            ArrayList<Node<Politician>> level = new ArrayList<>(queue.size());


            for (Node<Politician> node : new ArrayList<>(queue) ) {
                level.add(node);
                if (node.getData().getId().equals(goalId)) {
                    foundGoal = Boolean.TRUE;
                    goalAndOldest.add(node);
                }
                queue.addAll(node.getChildren());

                queue.poll();
            }

            if (level.size() == 1 && foundGoal)
                this.isOnlyChild = Boolean.TRUE;

            LocalDate earliestDate = LocalDate.MAX;
            Node<Politician> mostSenior = new Node<Politician>();

            //If the politician is found:
            //For each tree level, check the oldest member
            if (foundGoal) {
                for (Node<Politician> node : level) {
                    if (node.getData().getSeniority().isBefore(earliestDate)) {
                        earliestDate = node.getData().getSeniority();
                        mostSenior = node;
                    }
                }
                goalAndOldest.add(mostSenior);
                return goalAndOldest;
            }
        }
        return goalAndOldest;
    }

    public void promoteOldestSubordinate() {

    }

}
