package com.scotlandyard.scotlandyard;

import java.util.*;

public class Node<T> {

    private T data = null;

    private List<Node<T>> children = new ArrayList<>();

    private Node<T> parent = null;

    public Node() {

    }

    public Node(T data) {
        this.data = data;
    }

    public Node<T> addChild(Node<T> child) {
        child.setParent(this);
        this.children.add(child);
        return child;
    }

    public void addChildren(List<Node<T>> children) {
        children.forEach(each -> each.setParent(this));
        this.children.addAll(children);
    }

    public List<Node<T>> getChildren() {
        return children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private void setParent(Node<T> parent) {
        this.parent = parent;
    }

    public Node<T> getParent() {
        return parent;
    }

    public void deleteNode() {

        //If the node has a parent, assign all of their children to it
        if (parent != null) {
            int index = this.parent.getChildren().indexOf(this);
            this.parent.getChildren().remove(this);
            for (Node<T> each : getChildren()) {
                each.setParent(this.parent);
            }
            this.parent.getChildren().addAll(index, this.getChildren());
        } else {
            deleteRootNode();
        }
        this.getChildren().clear();
    }

    public List<Node<T>> deleteNodePromoteChild(Node<T> oldestChild) {

        List<Node<T>> childrenToUpdate = new ArrayList<>();
        //If the node has a parent, promote the oldest child
        if (parent != null) {
            //int index = this.parent.getChildren().indexOf(this);
            this.parent.getChildren().remove(this);
            oldestChild.setParent(this.parent);
            for (Node<T> each : getChildren()) {
                if (!each.equals(oldestChild)){
                    each.setParent(oldestChild);
                    oldestChild.addChild(each);
                    childrenToUpdate.add(each);
                }
            }
            this.parent.getChildren().add(oldestChild);

        //etChildren().addAll(index, oldestChild.getChildren());
        } else {
            deleteRootNode();
        }
        this.getChildren().clear();
        return childrenToUpdate;
    }

    public List<Node<T>> deleteNodeTransferChildren (Node<T> oldestBrother) {

        List<Node<T>> childrenToUpdate = new ArrayList<>();
        if (parent != null) {
            this.parent.getChildren().remove(this);
            for (Node<T> each : getChildren()) {
                each.setParent(oldestBrother);
                oldestBrother.addChild(each);
                childrenToUpdate.add(each);
            }

        } else {
            deleteRootNode();
        }
        this.getChildren().clear();
        return childrenToUpdate;
    }

    public Node<T> deleteRootNode() {
        if (parent != null) {
            throw new IllegalStateException("deleteRootNode not called on root");
        }
        Node<T> newParent = null;
        if (!getChildren().isEmpty()) {
            newParent = getChildren().get(0);
            newParent.setParent(null);
            getChildren().remove(0);
            for (Node<T> each : getChildren()) {
                each.setParent(newParent);
            }
            newParent.getChildren().addAll(getChildren());
        }
        this.getChildren().clear();
        return newParent;
    }


    public Node getRoot() {
        if(parent == null){
            return this;
        }
        return parent.getRoot();
    }

    /*public static <T> void printTree(Node<T> node, String appender) {
        System.out.println(appender + node.getData());
        node.getChildren().forEach(each ->  printTree(each, appender + appender));
    }*/

}
