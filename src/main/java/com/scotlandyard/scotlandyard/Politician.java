package com.scotlandyard.scotlandyard;

import java.time.LocalDate;
import java.util.Set;
import javax.persistence.*;

@Entity
public class Politician {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;
    @Column
    private String name;
    @Column
    private Integer idSup;
    @Column
    private LocalDate seniority;
    @Column
    private Boolean freedomStatus;

    @OneToMany(mappedBy = "id")
    private Set<Politician> subordinatesWhenJailed;

    public Politician(){

    }

    public Politician(Integer id, String name, Integer idSup, LocalDate seniority, Boolean freedomStatus) {

        this.id = id;
        this.name = name;
        this.idSup = idSup;
        this.seniority = seniority;
        this.freedomStatus = freedomStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getname() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdSup() {
        return idSup;
    }

    public void setIdSup(Integer idSup) { this.idSup = idSup; }

    public LocalDate getSeniority() {
        return seniority;
    }

    public void setSeniority(LocalDate seniority) {
        this.seniority = seniority;
    }

    public Boolean getFreedomStatus() {
        return freedomStatus;
    }

    public void setFreedomStatus(Boolean freedomStatus) {
        this.freedomStatus = freedomStatus;
    }

    @Override
    public String toString() {
        return String.format(
                "Politician[id=%d, name='%s', idSup=%d]",
                id, name, idSup);
    }
}
