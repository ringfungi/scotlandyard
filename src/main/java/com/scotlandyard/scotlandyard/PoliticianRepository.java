package com.scotlandyard.scotlandyard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class PoliticianRepository {
    @Autowired
    EntityManager em;

    public Collection<Politician> getFreePoliticians() {
        return this.em.createQuery("from Politician p WHERE p.freedomStatus is TRUE").getResultList();
    }
}
