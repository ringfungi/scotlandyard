package com.scotlandyard.scotlandyard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;

@SpringBootApplication
public class ScotlandyardApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScotlandyardApplication.class, args);
		/*Politician rootPol = new Politician(0,"ABC",0, LocalDate.of(2018,01,01), Boolean.TRUE);

		Node<Politician> root = new Node<Politician>(rootPol);

		Node<String> node1 = root.addChild(new Node<String>("node 1"));

		Node<String> node11 = node1.addChild(new Node<String>("node 11"));
		Node<String> node111 = node11.addChild(new Node<String>("node 111"));
		Node<String> node112 = node11.addChild(new Node<String>("node 112"));

		Node<String> node12 = node1.addChild(new Node<String>("node 12"));

		Node<String> node2 = root.addChild(new Node<String>("node 2"));

		Node<String> node21 = node2.addChild(new Node<String>("node 21"));
		Node<String> node211 = node2.addChild(new Node<String>("node 22"));
		return root;

		root.printTree(root, " ");*/
	}
}
