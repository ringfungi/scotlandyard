package com.scotlandyard.scotlandyard;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan
public class PoliticianRepositoryTests {

    @Autowired
    EntityManager em;

    @Autowired
    public PoliticianRepository politicianRepository;

    @Test
    @Transactional
    public void testGetFreePoliticians() {
        Politician rootPol = new Politician(0,"ABC",-1, LocalDate.of(2018,1,1), Boolean.TRUE);
        this.em.merge(rootPol);

        Collection<Politician> freePoliticians = this.politicianRepository.getFreePoliticians();

        assertThat(freePoliticians, hasSize(1));

    }
}
