package com.scotlandyard.scotlandyard;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.Mock;


import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(BlockJUnit4ClassRunner.class)
public class PoliticianTests {


    public <T> void printTree(Node<Politician> node, String appender) {
        System.out.println(appender + node.getData().getId() + ":" + node.getData().getIdSup());
        node.getChildren().forEach(each ->  printTree(each, appender + appender));
    }


    @Test
    public void testFoo() {

        Politician rootPol = new Politician(0,"Capo",-1, LocalDate.of(2018,1,1), Boolean.TRUE);

        Politician childPol1 = new Politician(20,"Tobias",0, LocalDate.of(2018,4,5), Boolean.TRUE);

        Politician childPol2 = new Politician(30,"Felix",0, LocalDate.of(2018,4,3), Boolean.TRUE);

        Politician childPol11 = new Politician(50,"Capanga1",20, LocalDate.of(2018,4,9), Boolean.TRUE);
        Politician childPol12 = new Politician(60,"Capanga2",20, LocalDate.of(2018,4,10), Boolean.TRUE);
        Politician childPol13 = new Politician(61,"Capanga3",20, LocalDate.of(2018,4,11), Boolean.TRUE);
        Politician childPol14 = new Politician(62,"Capanga4",20, LocalDate.of(2018,4,12), Boolean.TRUE);
        Politician childPol15 = new Politician(63,"Capanga5",20, LocalDate.of(2018,4,13), Boolean.TRUE);
        Politician childPol16 = new Politician(64,"Capanga6",20, LocalDate.of(2018,4,14), Boolean.TRUE);
        Politician childPol17 = new Politician(65,"Capanga7",20, LocalDate.of(2018,4,15), Boolean.TRUE);
        Politician childPol18 = new Politician(66,"Capanga8",20, LocalDate.of(2018,4,16), Boolean.TRUE);
        Politician childPol19 = new Politician(67,"Capanga9",20, LocalDate.of(2018,4,17), Boolean.TRUE);
        Politician childPol110 = new Politician(68,"Capanga10",20, LocalDate.of(2018,4,18), Boolean.TRUE);
        Politician childPol111 = new Politician(69,"Capanga11",20, LocalDate.of(2018,4,19), Boolean.TRUE);
        Politician childPol112 = new Politician(70,"Capanga12",20, LocalDate.of(2018,4,20), Boolean.TRUE);
        Politician childPol113 = new Politician(71,"Capanga13",20, LocalDate.of(2018,4,21), Boolean.TRUE);
        Politician childPol114 = new Politician(72,"Capanga14",20, LocalDate.of(2018,4,22), Boolean.TRUE);
        Politician childPol115 = new Politician(73,"Capanga15",20, LocalDate.of(2018,4,23), Boolean.TRUE);
        Politician childPol116 = new Politician(74,"Capanga16",20, LocalDate.of(2018,4,24), Boolean.TRUE);
        Politician childPol117 = new Politician(75,"Capanga17",20, LocalDate.of(2018,4,25), Boolean.TRUE);
        Politician childPol118 = new Politician(76,"Capanga18",20, LocalDate.of(2018,4,26), Boolean.TRUE);
        Politician childPol119 = new Politician(78,"Capanga20",20, LocalDate.of(2018,4,27), Boolean.TRUE);
        Politician childPol120 = new Politician(79,"Capanga21",20, LocalDate.of(2018,4,28), Boolean.TRUE);

        Node<Politician> root = new Node<>(rootPol);

        JailingService jService = new JailingService(root);

        Node<Politician> node1 = new Node<>(childPol1);
        Node<Politician> node2 = new Node<>(childPol2);
        Node<Politician> node11 = new Node<>(childPol11);
        Node<Politician> node12 = new Node<>(childPol12);
        Node<Politician> node13 = new Node<>(childPol13);
        Node<Politician> node14 = new Node<>(childPol14);
        Node<Politician> node15 = new Node<>(childPol15);
        Node<Politician> node16 = new Node<>(childPol16);
        Node<Politician> node17 = new Node<>(childPol17);
        Node<Politician> node18 = new Node<>(childPol18);
        Node<Politician> node19 = new Node<>(childPol19);
        Node<Politician> node110 = new Node<>(childPol110);
        Node<Politician> node111 = new Node<>(childPol111);
        Node<Politician> node112 = new Node<>(childPol112);
        Node<Politician> node113 = new Node<>(childPol113);
        Node<Politician> node114 = new Node<>(childPol114);
        Node<Politician> node115 = new Node<>(childPol115);
        Node<Politician> node116 = new Node<>(childPol116);
        Node<Politician> node117 = new Node<>(childPol117);
        Node<Politician> node118 = new Node<>(childPol118);
        Node<Politician> node119 = new Node<>(childPol119);
        Node<Politician> node120 = new Node<>(childPol120);

		root.addChild(node1);
        root.addChild(node2);
        node1.addChild(node11);
        node1.addChild(node12);
        node1.addChild(node13);
        node1.addChild(node14);
        node1.addChild(node15);
        node1.addChild(node16);
        node1.addChild(node17);
        node1.addChild(node18);
        node1.addChild(node19);
        node1.addChild(node110);
        node1.addChild(node111);
        node1.addChild(node112);
        node1.addChild(node113);
        node1.addChild(node114);
        node1.addChild(node115);
        node1.addChild(node116);
        node1.addChild(node117);
        node1.addChild(node118);
        node1.addChild(node119);
        node1.addChild(node120);

        jService.jail(childPol1);

        //jService.free(childPol11);


        printTree(root, " ");
    }
}
